package rs.vlasi.anex.anex.web.dto

import java.time.ZonedDateTime

data class CreatePersonRequest(
    val name: String,
    val address: String,
    val driverId: String
)

data class CreateCarRequest(
    val licence: String,
    val model: String,
    val year: String
)

data class CreateAccidentRequest(
    val accident: AccidentInformation,
    val carsLicences: Set<String>
)
data class OwnCarRequest(
    val personId: Long,
    val carLicence: String
)


data class AccidentInformation(
    val reportNumber: String,
    val location: String,
    val date: ZonedDateTime
)


data class UpdatePersonRequest(
    val name: String,
    val licenceId: String
)

