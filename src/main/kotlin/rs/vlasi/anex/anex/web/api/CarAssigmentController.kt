package rs.vlasi.anex.anex.web.api

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import rs.vlasi.anex.anex.domain.CarOwningService
import rs.vlasi.anex.anex.web.dto.AccidentResponse
import rs.vlasi.anex.anex.web.dto.OwnCarRequest
import rs.vlasi.anex.anex.web.dto.OwnCarResponse
import rs.vlasi.anex.anex.web.dto.RestResponse

@RestController
@RequestMapping("api/v1/person/assign/car")
class CarAssigmentController(
    private val carOwningService: CarOwningService
) {

    @PostMapping
    fun own(@RequestBody request: OwnCarRequest): RestResponse<OwnCarResponse> {
        return RestResponse(carOwningService.own(request))
    }


}
