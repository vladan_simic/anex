package rs.vlasi.anex.anex.web.api

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import rs.vlasi.anex.anex.domain.AccidentService
import rs.vlasi.anex.anex.web.dto.AccidentResponse
import rs.vlasi.anex.anex.web.dto.CreateAccidentRequest
import rs.vlasi.anex.anex.web.dto.RestResponse

@RestController
@RequestMapping("api/v1/accident")
class AccidentController(
    private val accidentService: AccidentService
) {

    @PostMapping
    fun create(@RequestBody request: CreateAccidentRequest): RestResponse<AccidentResponse> {
        return RestResponse(accidentService.createAccident(request))
    }
}
