package rs.vlasi.anex.anex.web.api

import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import rs.vlasi.anex.anex.config.checkers.LiveOnly
import rs.vlasi.anex.anex.domain.CarService
import rs.vlasi.anex.anex.web.dto.CarResponse
import rs.vlasi.anex.anex.web.dto.CreateCarRequest
import rs.vlasi.anex.anex.web.dto.RestResponse

@RestController
@RequestMapping("api/v1/cars")
class CarController(
    private val carService: CarService
) {

    @PostMapping
    fun create(@RequestBody request: CreateCarRequest): RestResponse<CarResponse> {
        return RestResponse(carService.createCar(request))
    }

    @GetMapping("/{id}")
    fun getByLicenceId(@PathVariable id: Long): RestResponse<CarResponse> {
        return RestResponse(carService.getCarById(id))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable id: Long, request: CreateCarRequest): RestResponse<CarResponse> {
        return RestResponse(carService.updateCar(id = id, editRequest = request))
    }

    @LiveOnly
    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: Long) {
        carService.deleteCar(id)
    }

}
