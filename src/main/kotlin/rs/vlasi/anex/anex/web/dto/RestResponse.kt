package rs.vlasi.anex.anex.web.dto

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class RestResponse<T>(
    val data: T,
    val meta: Map<String, Any> = emptyMap()
) {
    constructor(data: T) : this(data, emptyMap())
    constructor(data: T, vararg meta: Pair<String, Any>) : this(data, meta.toMap())
    constructor(data: T, meta: Collection<Pair<String, Any>>) : this(data, meta.toMap())

    companion object {
        @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
        @JvmStatic
        fun <T> createRestResponse(
            @JsonProperty("data") data: T,
            @JsonProperty("meta") meta: Map<String, Any>? = null
        ): RestResponse<T> {
            return RestResponse(data, meta ?: emptyMap())
        }
    }
}
