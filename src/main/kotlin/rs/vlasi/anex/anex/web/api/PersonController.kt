package rs.vlasi.anex.anex.web.api

import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import rs.vlasi.anex.anex.config.checkers.LiveOnly
import rs.vlasi.anex.anex.domain.PersonService
import rs.vlasi.anex.anex.web.dto.CreatePersonRequest
import rs.vlasi.anex.anex.web.dto.PersonResponseDTO
import rs.vlasi.anex.anex.web.dto.RestResponse

@RestController
@RequestMapping("api/v1/persons")
class PersonController(
    private val personService: PersonService
) {

    @PostMapping
    fun create(@RequestBody request: CreatePersonRequest): RestResponse<PersonResponseDTO> {
        return RestResponse(personService.createPerson(request))
    }

    @GetMapping("/{id}")
    fun getByLicenceId(@PathVariable id: Long): RestResponse<PersonResponseDTO> {
        return RestResponse(personService.getPersonById(id))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable id: Long, request: CreatePersonRequest): RestResponse<PersonResponseDTO> {
        return RestResponse(personService.updatePerson(id = id, editRequest = request))
    }

    @LiveOnly
    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: Long) {
        personService.deletePerson(id)
    }


}
