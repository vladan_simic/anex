package rs.vlasi.anex.anex.web.dto

data class PersonResponseDTO(
    val id: Long,
    val name: String,
    val address: String,
    val driverId: String
)


data class CarResponse(
    val id: Long,
    val licence: String,
    val model: String,
    val year: String
)


data class AccidentResponse(
    val id: Long,
    val carsInvolved: Set<String>,
    val accidentInformation: AccidentInformation
)


data class OwnCarResponse(
    val person:PersonResponseDTO,
    val owning:Set<CarResponse>
)
