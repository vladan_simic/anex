package rs.vlasi.anex.anex.data.repositories

import org.springframework.data.jpa.repository.JpaRepository
import rs.vlasi.anex.anex.data.model.AccidentModel
import rs.vlasi.anex.anex.data.model.ParticipationModel

interface ParticipationRepository : JpaRepository<ParticipationModel, Long> {
}
