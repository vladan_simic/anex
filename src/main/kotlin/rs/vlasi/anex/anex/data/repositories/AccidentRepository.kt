package rs.vlasi.anex.anex.data.repositories

import org.springframework.data.jpa.repository.JpaRepository
import rs.vlasi.anex.anex.data.model.AccidentModel

interface AccidentRepository : JpaRepository<AccidentModel, Long> {
}
