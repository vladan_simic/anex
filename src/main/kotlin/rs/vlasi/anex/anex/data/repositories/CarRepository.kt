package rs.vlasi.anex.anex.data.repositories

import org.springframework.data.jpa.repository.JpaRepository
import rs.vlasi.anex.anex.data.model.CarModel

interface CarRepository : JpaRepository<CarModel, Long> {

    fun findByLicence(licenceId: String): CarModel?

    fun findAllByLicenceIn(licences:List<String>):List<CarModel>?

}
