package rs.vlasi.anex.anex.data.repositories

import org.springframework.data.jpa.repository.JpaRepository
import rs.vlasi.anex.anex.data.model.CarOwnershipModel

interface CarOwnershipRepository : JpaRepository<CarOwnershipModel, Long> {

    fun findAllByPersonId(personId: Long): List<CarOwnershipModel>

}
