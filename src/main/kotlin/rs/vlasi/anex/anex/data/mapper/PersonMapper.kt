package rs.vlasi.anex.anex.data.mapper

import rs.vlasi.anex.anex.data.model.PersonModel
import rs.vlasi.anex.anex.exceptions.InternalServerError
import rs.vlasi.anex.anex.web.dto.CreatePersonRequest
import rs.vlasi.anex.anex.web.dto.PersonResponseDTO

object PersonMapper {

    fun toPersonModel(request: CreatePersonRequest): PersonModel {
        return PersonModel(
            name = request.name,
            address = request.address,
            driverId = request.driverId
        )

    }

    fun toPersonResponse(model: PersonModel): PersonResponseDTO {
        return PersonResponseDTO(
            id = model.id ?: throw InternalServerError("Illegal model state $model"),
            name = model.name ?: throw InternalServerError("Illegal model state $model"),
            address = model.address ?: throw InternalServerError("Illegal model state $model"),
            driverId = model.driverId ?: throw InternalServerError("Illegal model state $model")
        )

    }


}
