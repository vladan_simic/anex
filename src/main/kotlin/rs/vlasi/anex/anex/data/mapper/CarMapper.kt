package rs.vlasi.anex.anex.data.mapper

import rs.vlasi.anex.anex.data.model.CarModel
import rs.vlasi.anex.anex.data.model.PersonModel
import rs.vlasi.anex.anex.exceptions.InternalServerError
import rs.vlasi.anex.anex.web.dto.CarResponse
import rs.vlasi.anex.anex.web.dto.CreateCarRequest
import rs.vlasi.anex.anex.web.dto.CreatePersonRequest
import rs.vlasi.anex.anex.web.dto.PersonResponseDTO

object CarMapper {

    fun toCarModel(request: CreateCarRequest): CarModel {
        return CarModel(
           model = request.model,
           licence = request.licence,
           year = request.year
        )

    }

    fun toCarResponse(model: CarModel): CarResponse {
        return CarResponse(
            id = model.id ?: throw InternalServerError("Illegal model state $model"),
            model = model.model ?: throw InternalServerError("Illegal model state $model"),
            licence = model.licence ?: throw InternalServerError("Illegal model state $model"),
            year = model.year ?: throw InternalServerError("Illegal model state $model")
        )

    }


}
