package rs.vlasi.anex.anex.data.model

import java.time.ZonedDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "person")
class PersonModel(
    @Id @GeneratedValue
    var id: Long? = null,
    @Column(name = "name")
    var name: String? = null,
    @Column(name = "driver_id", unique = true)
    var driverId: String? = null,
    @Column(name = "address")
    var address: String? = null,
    @OneToMany(mappedBy = "person")
    var carOwnership: Set<CarOwnershipModel>? = null
)

@Entity
@Table(name = "car")
class CarModel(
    @Id @GeneratedValue
    var id: Long? = null,
    @Column(name = "model")
    var model: String? = null,
    @Column(name = "licence", unique = true)
    var licence: String? = null,
    @Column(name = "year")
    var year: String? = null,
    @OneToMany(mappedBy = "car")
    var carOwnership: Set<CarOwnershipModel>? = null
)

@Entity
@Table(name = "car_ownership")
class CarOwnershipModel(
    @Id @GeneratedValue
    var id: Long? = null,
    @ManyToOne
    @JoinColumn(name = "person_id")
    var person: PersonModel? = null,
    @ManyToOne
    @JoinColumn(name = "car_id")
    var car: CarModel? = null,
    @OneToMany(mappedBy = "car")
    var participations: Set<ParticipationModel> = emptySet()
)

@Entity
@Table(name = "participation")
class ParticipationModel(
    @Id @GeneratedValue
    var id: Long? = null,
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_id")
    var car: CarModel? = null,
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "accident_id")
    var accident: AccidentModel? = null,
    @Column(name = "damage_amount")
    var damageAmount: Double? = null
)

@Entity
@Table(name = "accident")
class AccidentModel(
    @Id @GeneratedValue
    var id: Long? = null,
    @Column(name = "report_number", unique = true)
    var reportNumber: String? = null,
    @Column(name = "location")
    var location: String? = null,
    @Column(name = "date")
    var date: ZonedDateTime? = null,
    @OneToMany(mappedBy = "accident",fetch = FetchType.LAZY)
    var participations: Set<ParticipationModel>? = null
)






