package rs.vlasi.anex.anex.config.checkers

import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import rs.vlasi.anex.anex.exceptions.NotAllowed

@Component
@Aspect
class LiveOnlyModeChecker(
    private val environment: Environment
) {


    //it could also be done via @Profile but this one does the same thing
    // we would just hide the spring specificness

    @Before("@annotation(LiveOnly)")
    fun fullDesignerEnabled(joinPoint: JoinPoint) {

        if (environment
                .activeProfiles
                .contains("production")
        ) {
            throw NotAllowed("Not allowed to perform given action in production env")
        }
    }

}
