package rs.vlasi.anex.anex.config.security

import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableConfigurationProperties
class BasicAuthSecurityConfiguration : WebSecurityConfigurerAdapter() {

    private val log = LoggerFactory.getLogger(BasicAuthSecurityConfiguration::class.java)

    override fun configure(http: HttpSecurity) {

        log.info("\t>> disabling csrf")
        log.info("\t>> authorize all requests")
        log.info("\t>> set auth-method to basic authentication")

        http
            .csrf().disable()
            .authorizeRequests().anyRequest().authenticated()
            .and()
            .httpBasic()
    }

}
