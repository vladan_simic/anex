package rs.vlasi.anex.anex.config.profiles

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

@Configuration
@Profile("production")
class ProductionProfile {
    //any production specific configuration
}
