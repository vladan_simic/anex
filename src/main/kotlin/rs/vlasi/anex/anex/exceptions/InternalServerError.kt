package rs.vlasi.anex.anex.exceptions

import java.lang.RuntimeException

open class InternalServerError(message: String?) : RuntimeException(message)
