package rs.vlasi.anex.anex.exceptions

import java.lang.RuntimeException

open class NotFound(message: String?) : RuntimeException(message) {
}
