package rs.vlasi.anex.anex.exceptions

import java.lang.RuntimeException

open class NotAllowed(message: String?) : RuntimeException(message) {
}
