package rs.vlasi.anex.anex

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AnexApplication

fun main(args: Array<String>) {
	runApplication<AnexApplication>(*args)
}
