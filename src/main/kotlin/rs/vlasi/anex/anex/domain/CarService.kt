package rs.vlasi.anex.anex.domain

import rs.vlasi.anex.anex.web.dto.CarResponse
import rs.vlasi.anex.anex.web.dto.CreateCarRequest
import rs.vlasi.anex.anex.web.dto.CreatePersonRequest
import rs.vlasi.anex.anex.web.dto.PersonResponseDTO

interface CarService {

    fun createCar(request: CreateCarRequest): CarResponse

    fun getCarById(id: Long): CarResponse

    fun updateCar(id: Long, editRequest: CreateCarRequest): CarResponse

    fun deleteCar(id: Long)

}
