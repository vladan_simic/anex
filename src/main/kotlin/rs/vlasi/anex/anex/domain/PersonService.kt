package rs.vlasi.anex.anex.domain

import rs.vlasi.anex.anex.web.dto.CreatePersonRequest
import rs.vlasi.anex.anex.web.dto.PersonResponseDTO

interface PersonService {

    fun createPerson(request: CreatePersonRequest): PersonResponseDTO

    fun getPersonById(id: Long): PersonResponseDTO

    fun updatePerson(id: Long, editRequest: CreatePersonRequest): PersonResponseDTO

    fun deletePerson(id: Long)

}
