package rs.vlasi.anex.anex.domain

import rs.vlasi.anex.anex.web.dto.AccidentResponse
import rs.vlasi.anex.anex.web.dto.CreateAccidentRequest

interface AccidentService {

    fun createAccident(request: CreateAccidentRequest): AccidentResponse

    fun getAccident(id: Long): AccidentResponse


}
