package rs.vlasi.anex.anex.domain.impl

import org.springframework.stereotype.Service
import rs.vlasi.anex.anex.data.mapper.CarMapper
import rs.vlasi.anex.anex.data.mapper.PersonMapper
import rs.vlasi.anex.anex.data.model.CarOwnershipModel
import rs.vlasi.anex.anex.data.repositories.CarOwnershipRepository
import rs.vlasi.anex.anex.data.repositories.CarRepository
import rs.vlasi.anex.anex.data.repositories.PersonRepository
import rs.vlasi.anex.anex.domain.CarOwningService
import rs.vlasi.anex.anex.exceptions.NotFound
import rs.vlasi.anex.anex.web.dto.OwnCarRequest
import rs.vlasi.anex.anex.web.dto.OwnCarResponse
import javax.transaction.Transactional

@Service
@Transactional
class CarOwnershipServiceImpl(
    val carRepository: CarRepository,
    val personRepository: PersonRepository,
    val carOwnershipRepository: CarOwnershipRepository
) : CarOwningService {


    override fun own(request: OwnCarRequest): OwnCarResponse {
        val personModel = personRepository.getById(request.personId)
        val car = carRepository.findByLicence(request.carLicence) ?: throw NotFound("No car with given licence")

        carOwnershipRepository.save(
            CarOwnershipModel(
                person = personModel,
                car = car
            )
        )

        val carsOwned = carOwnershipRepository.findAllByPersonId(personId = personModel.id!!).map { it.car }


        return OwnCarResponse(
            person = PersonMapper.toPersonResponse(personModel),
            owning = carsOwned
                .filterNotNull()
                .map { CarMapper.toCarResponse(it) }
                .toSet()
        )
    }
}
