package rs.vlasi.anex.anex.domain.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import rs.vlasi.anex.anex.data.mapper.PersonMapper
import rs.vlasi.anex.anex.data.repositories.PersonRepository
import rs.vlasi.anex.anex.domain.PersonService
import rs.vlasi.anex.anex.exceptions.NotFound
import rs.vlasi.anex.anex.web.dto.CreatePersonRequest
import rs.vlasi.anex.anex.web.dto.PersonResponseDTO
import javax.transaction.Transactional

@Service
@Transactional
class PersonServiceImpl(
    val personRepository: PersonRepository
) : PersonService {

    private val log: Logger = LoggerFactory.getLogger(PersonServiceImpl::class.java)

    override fun createPerson(request: CreatePersonRequest): PersonResponseDTO {
        return PersonMapper.toPersonResponse(
            personRepository.save(
                PersonMapper
                    .toPersonModel(request)
            )
        )
    }

    override fun getPersonById(id: Long): PersonResponseDTO {
        val personModel = personRepository.findById(id).orElseThrow { NotFound("Person with id $id not found") }

        return PersonMapper.toPersonResponse(personModel)
    }

    override fun updatePerson(id: Long, editRequest: CreatePersonRequest): PersonResponseDTO {
        val personModel = personRepository.findById(id).orElseThrow { NotFound("Person with id $id not found") }

        personModel.address = editRequest.address
        personModel.name = editRequest.name
        personModel.driverId = editRequest.driverId

        log.debug("Updating user with $id new values: $editRequest")

        return PersonMapper.toPersonResponse(personModel)
    }

    override fun deletePerson(id: Long) {
        log.debug("Deleting user with id = $id")

        personRepository.deleteById(id)
    }
}
