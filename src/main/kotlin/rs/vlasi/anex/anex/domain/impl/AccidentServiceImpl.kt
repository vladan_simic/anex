package rs.vlasi.anex.anex.domain.impl

import org.springframework.stereotype.Service
import rs.vlasi.anex.anex.data.model.AccidentModel
import rs.vlasi.anex.anex.data.model.ParticipationModel
import rs.vlasi.anex.anex.data.repositories.AccidentRepository
import rs.vlasi.anex.anex.data.repositories.CarRepository
import rs.vlasi.anex.anex.data.repositories.ParticipationRepository
import rs.vlasi.anex.anex.domain.AccidentService
import rs.vlasi.anex.anex.exceptions.NotFound
import rs.vlasi.anex.anex.web.dto.AccidentInformation
import rs.vlasi.anex.anex.web.dto.AccidentResponse
import rs.vlasi.anex.anex.web.dto.CreateAccidentRequest
import javax.transaction.Transactional

@Service
@Transactional
class AccidentServiceImpl(
    private val accidentRepository: AccidentRepository,
    private val carRepository: CarRepository,
    private val participationRepository: ParticipationRepository
) : AccidentService {

    override fun createAccident(request: CreateAccidentRequest): AccidentResponse {
        val accident = accidentRepository.save(
            AccidentModel(
                location = request.accident.location,
                reportNumber = request.accident.reportNumber,
                date = request.accident.date,
                participations = emptySet()
            )
        )

        val carsInvolved = carRepository.findAllByLicenceIn(request.carsLicences.toList())

        val participationsToSave = carsInvolved?.map {
            ParticipationModel(
                car = it,
                accident = accident,
                damageAmount = 42.0
            )
        }?.toMutableList()


        if (!participationsToSave.isNullOrEmpty()) {
            val participationsModel = participationRepository.saveAll(participationsToSave)
            accident.participations = participationsModel.toSet()

        }


        return AccidentResponse(
            id = accident.id!!,
            accidentInformation = request.accident,
            carsInvolved = request.carsLicences
        )
    }

    override fun getAccident(id: Long): AccidentResponse {
        val accident = accidentRepository.findById(id).orElseThrow { NotFound("No such accident") }
        return AccidentResponse(
            id = accident.id!!,
            accidentInformation = AccidentInformation(
                location = accident.location!!,
                date = accident.date!!,
                reportNumber = accident.reportNumber!!
            ),
            carsInvolved = accident.participations.orEmpty().mapNotNull { it.car?.licence }.toSet()
        )
    }
}
