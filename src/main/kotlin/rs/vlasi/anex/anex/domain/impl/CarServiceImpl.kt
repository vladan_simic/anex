package rs.vlasi.anex.anex.domain.impl

import org.springframework.stereotype.Service
import rs.vlasi.anex.anex.data.mapper.CarMapper
import rs.vlasi.anex.anex.data.repositories.CarRepository
import rs.vlasi.anex.anex.domain.CarService
import rs.vlasi.anex.anex.exceptions.NotFound
import rs.vlasi.anex.anex.web.dto.CarResponse
import rs.vlasi.anex.anex.web.dto.CreateCarRequest
import javax.transaction.Transactional

@Service
@Transactional
class CarServiceImpl(
    val carRepository: CarRepository
) : CarService {

    override fun createCar(request: CreateCarRequest): CarResponse {
        return CarMapper.toCarResponse(carRepository.save(CarMapper.toCarModel(request)))
    }

    override fun getCarById(id: Long): CarResponse {
        return CarMapper.toCarResponse(carRepository.findById(id).orElseThrow { NotFound("No car with given id") })
    }

    override fun updateCar(id: Long, editRequest: CreateCarRequest): CarResponse {
        TODO("Not yet implemented")
    }

    override fun deleteCar(id: Long) {
        TODO("Not yet implemented")
    }
}
