package rs.vlasi.anex.anex.domain

import rs.vlasi.anex.anex.web.dto.OwnCarRequest
import rs.vlasi.anex.anex.web.dto.OwnCarResponse
import rs.vlasi.anex.anex.web.dto.PersonResponseDTO

interface CarOwningService {

    fun own(request: OwnCarRequest): OwnCarResponse

}
