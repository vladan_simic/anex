package rs.vlasi.anex.anex.data.repositories

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import rs.vlasi.anex.anex.data.model.PersonModel

@DataJpaTest
class RepositoriesTest @Autowired constructor(
    val entityManager: TestEntityManager,
    val personRepository: PersonRepository
) {

    @Nested
    inner class PersonRepo {

        @Test
        fun `should find user by id`() {

            entityManager.persist(
                PersonModel(
                    name = "Vladan",
                    address = "Jasenovo",
                    driverId = "myId"
                )
            )


            val gateFound =
                personRepository.findById(
                    1
                ).get()


            Assertions.assertEquals(
                "Vladan",
                gateFound.name
            )


        }
    }


}
