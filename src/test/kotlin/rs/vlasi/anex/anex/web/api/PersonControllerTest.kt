package rs.vlasi.anex.anex.web.api


import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import rs.vlasi.anex.anex.domain.PersonService
import rs.vlasi.anex.anex.exceptions.NotFound
import rs.vlasi.anex.anex.web.dto.PersonResponseDTO
import rs.vlasi.anex.anex.web.handlers.GlobalExceptionHandler

internal class PersonControllerTest {

    private lateinit var mockMvc: MockMvc

    private val personService: PersonService = mockk()

    @AfterEach
    fun reset() {
        clearAllMocks()
    }


    @BeforeEach
    fun setup() {
        mockMvc = MockMvcBuilders
            .standaloneSetup(PersonController(personService))
            .setControllerAdvice(GlobalExceptionHandler())
            .build()
    }

    @Nested
    inner class GetPerson {

        @Test
        fun `should return 404 status when no person exists`() {
            every {
                personService.getPersonById(1)
            }.throws(NotFound("No resource"))

            mockMvc
                .perform(
                    MockMvcRequestBuilders.get(
                        "/api/v1/persons/1"
                    )
                )
                .andExpect(
                    MockMvcResultMatchers.status().isNotFound
                )
        }

        @Test
        fun `should return 200 status when person exists`() {
            every {
                personService.getPersonById(1)
            }.returns(
                PersonResponseDTO(
                    id = 1,
                    name ="Vladan",
                    address = "Jasenovo",
                    driverId = "DL123!"
                )
            )

            mockMvc
                .perform(
                    MockMvcRequestBuilders.get(
                        "/api/v1/persons/1"
                    )
                )
                .andExpect(
                    MockMvcResultMatchers.status().isOk
                )

        }
    }



}
