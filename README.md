# Read Me First

- Project represents part of task for InsureQ.
  It has 4 main functionalities
    - Crud operations on person
    - Crud operations on car
    - Assign car to a person - Not completed
    - instantiate accident - Not completed

Assumption:
No Initial data is added 

# Running instructions

    Prerequisite:
        - Java 8

    - Build and Start Application:
    `gradlew build`
    java -jar /build/libs/anex-0.0.1-SNAPSHOT.jar


### Technologies/Tools

- [Spring Boot]
- [Spring Data]
- [Kotlin]

### Additional improvements

- Add Open API specification 
- Dockerize application ( use gradle to generate docker file via task)
- Add tests , service layer and integration test. Unit test also required for mapper
- Review model design

